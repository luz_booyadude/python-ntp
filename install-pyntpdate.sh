#! /bin/ash

# Get current path
FOLDERPATH="$(pwd)"

# Copying files
cp -v ntpdate.py /usr/sbin/
cp -v pyntpdate /etc/init.d/
chmod 755 /usr/sbin/ntpdate.py
chmod 755 /etc/init.d/pyntpdate
update-rc.d pyntpdate defaults

# Install ntplib
pip install ntplib-0.3.3.tar.gz