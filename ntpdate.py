import sys
import os
import os.path
import time
import datetime
import socket
from time import ctime
from sys import platform

try:
    from pip import main as pipmain
except:
    from pip._internal import main as pipmain

filename = os.path.basename(__file__)
LOG_PATH = "pyntpdate.log"
timeFormat = '%Y%m%d%H%M%S'
currentPath = os.getcwd()

def install(package):
    pipmain(['install', package])  # Currently use absolute name. Later may change to dynamic name.

def write_log(msg):
    log_data = "[" + datetime.datetime.now().strftime(timeFormat) + "] " + msg + "\n"
    print(log_data)
    with open(LOG_PATH, "a") as logfile:
        logfile.write(log_data)
    
try:
    import ntplib
except ImportError:
    write_log("ntplib module is not installed. Installing the module...\n")
    install(currentPath + "/ntplib-0.3.3.tar.gz")
    try:
        import ntplib
        write_log("ntplib modules installed.")
    except:
        write_log("ntplib modules failed to install. Exiting...")
        sys.exit(1)

if len(sys.argv) == 2:
    write_log("No port specified. Using default NTP port (123)...")
    SERVER=sys.argv[1]
    PORT='123'  # Default NTP port
elif len(sys.argv) == 3:
    SERVER=sys.argv[1]
    PORT=sys.argv[2]  # Default NTP port
else:
    write_log('usage: {0} <server address> <port>'.format(filename))
    sys.exit(1)

# Print out the config
write_log('SERVER = {0}'.format(SERVER))
write_log('PORT = {0}'.format(PORT))

# Check for log existence. If not exists, create new one.
if not os.path.isfile(LOG_PATH):
    # Create new log file
    os.system('touch ' + LOG_PATH)

port = sys.argv[1]
retry = 0

c = ntplib.NTPClient()

# Sleep for 30s to ensure the network is up and running
write_log("Waiting for network ready...")
time.sleep(30)

while True:
    try:
        response = c.request(SERVER, port=PORT)
        write_log(ctime(response.tx_time))
        timeEpoch=response.tx_time
        if platform == "linux" or platform == "linux2":
            timeString=time.strftime('%Y-%m-%d %H:%M:%S', time.localtime(timeEpoch))
            os.system('date -s \"' + timeString + '\"')
            write_log("New time is set. Updating in 6 hours...")
        elif platform == "win32":
            timeString=time.strftime('%d-%m-%Y %H:%M:%S', time.localtime(timeEpoch))
            timeWin32 = timeString.split()
            os.system('date ' + timeWin32[0])
            os.system('time ' + timeWin32[1])
            write_log("New time is set. Updating in 6 hours...")
        time.sleep(21600)
    except socket.error:
        write_log("Port access was refused. Change the port to accessible one.")
        if retry < 3:
            write_log("Retrying in 30 seconds...")
            retry += 1
            time.sleep(30)
        else:
            write_log("Still failed after 3 attempts.")
            write_log("Exiting...")
            break
    except ntplib.NTPException:
        write_log("No response received. Server may down or not exists.")
        write_log("Retrying in 1 minute...")
        time.sleep(60)
    except KeyboardInterrupt:
        write_log("Exiting...")
        break